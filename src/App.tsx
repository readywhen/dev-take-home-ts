import React from 'react';
import logo from './logo.svg';
// import { Country } from './features/country/Country';
import './App.css';
import expected0 from './expected0.png';
import expected1 from './expected1.png';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {/* <Country /> */}
        <p>
          TODOs:
          <ol style={{textAlign: 'left'}}>
            <li>Make a {'<Country/>'} Component below ReadyWhen Logo</li>
            <li>In that Component, Add a button with text "Fetch North American countries"</li>
            <li>Show a loading... indicator after click</li>
            <li>REST API call to fetch all the North American countries. (Use this: https://restcountries.com/)</li>
            <li>Display One dropdown with all the countries</li>
            <li>After selecting a country, show the country's flag and name below</li>
            <li>Display another dropdown with all the translations available</li>
            <li>On translation change, update the country name with selected translation</li>
            <li>Must use TypeScript</li>
            <li>Must use Redux Toolkit for state management</li>
          </ol>
        </p>

        <p>Expected result:</p>
        <img src={expected0} width="630"/>
        <img src={expected1} width="630"/>
        <p></p>
        <span>
          <span>Learn </span>
          <a
            className="App-link"
            href="https://reactjs.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            React
          </a>
          <span>, </span>
          <a
            className="App-link"
            href="https://redux.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Redux
          </a>
          <span>, </span>
          <a
            className="App-link"
            href="https://redux-toolkit.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Redux Toolkit
          </a>
          ,<span> and </span>
          <a
            className="App-link"
            href="https://react-redux.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            React Redux
          </a>
        </span>
      </header>
    </div>
  );
}

export default App;
